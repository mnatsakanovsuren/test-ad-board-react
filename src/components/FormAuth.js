import React, { Component } from 'react';

class FormAuth extends Component {
    state = {
        user: {
            username: '',
            password: ''
        },
        errors: {}

    };

    componentWillReceiveProps(nextProps){
        this.setState({
            errors: nextProps.errors
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        const errors = this.validate(this.state.user);
        this.setState({errors});
        if(Object.keys(errors).length === 0) {
            this.props.submit(this.state.user);
        }
    }
    validate = user => {
        let errors = {};
        if (!user.username) errors.username = "can't be empty";
        if (!user.password) errors.password = "can't be empty pass";
        return errors;
    }
    handleChange = event => {
        this.setState({
            user: {
                ...this.state.user,
                [event.target.name]: event.target.value
            }
        });
    }
    render() {
        return (
            <div className="form__wrapper">
                <form onSubmit={this.handleSubmit}>
                    <label>Username
                        <input type="text"
                               name="username"
                               id="username"
                               value={this.state.user.username}
                               onChange={this.handleChange}/>
                    </label>

                    {this.state.errors.username && (
                        <div>{this.state.errors.username}</div>
                    )}

                    <label>Password
                        <input type="password"
                               name="password"
                               id="password"
                               value={this.state.user.password}
                               onChange={this.handleChange}/>
                    </label>
                    {this.state.errors.password && (
                        <div>{this.state.errors.password}</div>
                    )}
                    {this.state.errors.global && (
                        <div>{this.state.errors.global}</div>
                    )}


                    <button type="submit">
                        <span>Log in</span>
                    </button>
                </form>
            </div>
        );
    }
}

export default FormAuth;
