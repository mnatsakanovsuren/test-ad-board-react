import React, { Component } from 'react';
import '../App.css';
import MainPage from "./MainPage";
import PostsFormPage from "./PostsFormPage";
import {Route} from 'react-router-dom';
import UserRoute from './routes/UserRoute'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Route path = "/" exact component={MainPage}/>
        <Route path = "/edit" exact component={UserRoute(PostsFormPage)}/>
      </div>
    );
  }
}

export default App;
