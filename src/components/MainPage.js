import React, { Component } from 'react';
import FormAuth from './FormAuth';
import {login, logOut} from '../actions/userActions';
import {connect} from  "react-redux";

class MainPage extends Component {
    state = {
        user: this.props.user.username ? this.props.user.username : '',
        errors: ''
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.user.username) {
            this.setState({
                user: nextProps.user.username
            });

        }
        this.setState({
            errors: nextProps.errors
        })
    }

    submit = data => this.props.login(data);
    logOut = () => {
        this.setState({
            user: ''
        });
        this.props.logOut();
    };

    render() {
        return (
            (this.state.user === '') ? <FormAuth submit = {this.submit} errors = {this.state.errors}/> : <div className="log-out" onClick={this.logOut}>Log-out {this.state.user}</div>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.user,
        errors: state.errors
    }
};

export default connect(mapStateToProps, {login, logOut})(MainPage);